#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "memmanage.h"

typedef struct MMspaceblock {
    void * data;
    unsigned long size;
    unsigned long nEntries;
    char * file; 
    unsigned long line;
} MMspaceblock;

typedef struct MMspacetable {
    MMspaceblock * blocks;
    unsigned long numberofblocks;
} MMspacetable;


MMspacetable* mem_man_new(unsigned long numberofblocks) {
    unsigned long idx;
    MMspacetable * memtable = malloc(sizeof(MMspacetable));
    if(memtable == NULL) {
        fprintf(stderr, "Could not allocate space for MMspacetable\n");
    }
    memtable->blocks = malloc(sizeof(MMspaceblock) * numberofblocks);
    if(memtable->blocks == NULL) {
        fprintf(stderr, "Could not allocate space for MMspaceblock\n");
    }
    memtable->numberofblocks = numberofblocks;

    for (idx = 0; idx < numberofblocks; ++idx) {

        memtable->blocks[idx].data = NULL;
    } 
    return memtable;
}

MMspaceblock* findBlock(MMspacetable *st, void * ptr) {
    unsigned long idx;
    for (idx = 0; idx < st->numberofblocks; ++idx) {
        if (ptr == st->blocks[idx].data) {
            return st->blocks + idx;
        }
    }
    return NULL;
}

void *mem_man_alloc(MMspacetable *st, char *file, unsigned long line,
                    void *ptr, unsigned long size, unsigned long number) {

    MMspaceblock* block = findBlock(st, ptr);
    if(block == NULL) {
        fprintf(stderr, "No free space in MMspacetable");
    }
    block->data = realloc(ptr, size * number);
    if(block->data == NULL) {
        fprintf(stderr, 
        "Could not allocate space for file %s in line %lu", file, line);
    }
    block->file = file;
    block->line = line; 
    block->nEntries = number;
    block->size = size;
    
    return block->data;
}

void mem_man_delete_ptr(MMspacetable *st, char *file, unsigned long line,
                        void *ptr) {
    if(ptr == NULL) {
        fprintf(stderr, "Cannot delete NULL pointer from MMspacetable");
    }
    MMspaceblock* block = findBlock(st, ptr);
    free(block->data);
    block->data = NULL;
    block->file = file;
    block->line = line;
}

void meme_man_info(const MMspacetable *st)
{
    unsigned long idx;
    //printf("Print out internal memory table:\n");
    for(idx = 0; idx < st->numberofblocks; ++idx)
    {
        if(st->blocks[idx].data != NULL) {
            printf("# active block %lu: allocated in file \"%s\", line %lu\n", 
            idx, st->blocks[idx].file, st->blocks[idx].line);      
        }
    }
}

void mem_man_check(const MMspacetable *st)
{
    unsigned long idx;
    for(idx = 0; idx < st->numberofblocks; ++idx) {
        if(st->blocks[idx].data != NULL) {
            fprintf(stderr, "space leak: main memory for block %lu not freed\n"
                    "%lu cells of size %lu\n"
                    "allocated: file \"%s\", line %lu\n",
                    idx, st->blocks[idx].nEntries, st->blocks[idx].size,   
                    st->blocks[idx].file, st->blocks[idx].line);        
            exit(EXIT_FAILURE);
        }
    }
}

void mem_man_delete(MMspacetable *st)
{
    unsigned long idx;
    for(idx = 0; idx < st->numberofblocks; ++idx)
    {
        free(st->blocks[idx].data);
    }
    free(st->blocks);
    free(st);
}
